import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import UsersScreen from "./src/usersScreen";
import UsersInfoScreen from "./src/userInfoScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Users" component={UsersScreen}/>
      <Stack.Screen name="Infos" component={UsersInfoScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}



