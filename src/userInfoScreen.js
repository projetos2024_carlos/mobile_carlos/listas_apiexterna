import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const UsersInfoScreen = ({ route }) => {
   const {infos} = route.params;

   return(
      <View>
         <Text>Detalhes da Tarefa:</Text>
         <Text> Nome: {infos.name}</Text>
         <Text> Apelido: {infos.username}</Text>
         <Text> E-mail: {infos.email}</Text>
         <Text> Cidade: {infos.address.city}</Text>
         <Text> Rua: {infos.address.street}</Text>
         <Text> Codigo Postal: {infos.address.zipcode}</Text>
         <Text> Telefone: {infos.phone}</Text>
         <Text> website: {infos.website}</Text>
         <Text> Nome da empresa: {infos.company.name}</Text>
         <Text> Slogan da empresa: {infos.company.catchPhrase}</Text>
         
      </View>
   )
}

export default UsersInfoScreen;