import React from "react";
import { Button, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";
import { useState, useEffect } from "react";
import { Text } from "react-native";

const UsersScreen = ({ navigation }) => {
   const [users, setUsers] = useState([]);

   useEffect(() => {
      getUsers()
   }, [])

   async function getUsers() {
      try {
         const response = await axios.get("https://jsonplaceholder.typicode.com/users")
         setUsers(response.data);
      } catch (error) {

      }
   }
   const UserPress = (infos) => {
      navigation.navigate("Infos", {infos})
   }

   return (
      <View>
         <Text> Lista de Usuários:</Text>
         <FlatList
            data={users}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
                  <TouchableOpacity onPress={() => UserPress(item)}> {item.name}</TouchableOpacity>
               
            )}
         />
      </View>
   )
};

export default UsersScreen;
